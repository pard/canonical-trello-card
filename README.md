# Trello Card Add

This project exists as a coding test for the Release Automation Software
Engineering position with Canonical. This small app will create a card in
Trello, on a specific list, on a specific board. If either the list or the
board doesn't exist it will be created at the same time. A card can be given
the following pieces of information:

- Name
- Description
- Label
- Comments

## The Trello API and Data Structure

Trello has three primary data structures:

1. Boards
2. Lists
3. Cards

A board holds lists, a list holds cards, and a card holds the specific data
about a _thing_ -- name, description, labels, etc. In order to create a card,
we will need to have some understanding of the other two data structures and
interact with them

## Usage

1. Add dependencies:

   ```bash
   pip install -r requirements.txt
   ```

2. Create a `settings.yml` file and add your [Trello key and token][0]:

   ```yaml
   trello_card_add:
     key: trello_key
     token: trello_token
   ```

3. Run the app to add a card!

   ```shell
   usage: main.py [-h] -b BOARD_NAME -l LIST_NAME -c CARD_NAME [-d CARD_DESC] [-L CARD_LABELS [CARD_LABELS ...]] [-C CARD_COMMENT]
                  [-s SETTINGS_FILE]

   optional arguments:
     -h, --help            show this help message and exit
     -b BOARD_NAME, --board BOARD_NAME
                           Board name (default: None)
     -l LIST_NAME, --list LIST_NAME
                           List name (default: None)
     -c CARD_NAME, --card CARD_NAME
                           Card name (default: None)
     -d CARD_DESC, --desc CARD_DESC
                           Card description (default: )
     -L CARD_LABELS [CARD_LABELS ...], --labels CARD_LABELS [CARD_LABELS ...]
                           Card labels (default: [])
     -C CARD_COMMENT, --comment CARD_COMMENT
                           Card comment (default: )
     -s SETTINGS_FILE, --settings SETTINGS_FILE
                           Settings file (default: settings.yml)
   ```

[0]: https://trello.com/app-key
