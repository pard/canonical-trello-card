class Board:
    def __init__(self, name="", board_id=""):
        self.name = name
        self.id = board_id

    def payload(self):
        payload = {}
        if self.name:
            payload["name"] = self.name
        if self.id:
            payload["id"] = self.id
        return payload
