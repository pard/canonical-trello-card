class List:
    def __init__(self, name, board, list_id=""):
        self.board = board
        self.id = list_id
        self.name = name

    def payload(self):
        payload = {
            "idBoard": self.board.id,
            "name": self.name,
        }
        return payload
