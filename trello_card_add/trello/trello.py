import random
import requests


class Trello:
    def __init__(self, key, token):
        self.url_base = "https://api.trello.com/1"
        self.headers = {"Accept": "application/json"}
        self.key = key
        self.token = token

    def add_card(self, board, lst, card):
        self.board = board
        self.list = lst
        self.card = card
        board_status = self._check_board()
        if not board_status:
            raise ValueError("Board could not be found or created.")
        list_status = self._check_list()
        if not list_status:
            raise ValueError("List could not be found or created.")
        label_status = self._check_labels()
        if not label_status:
            raise ValueError("Labels could not be found or created.")
        card_status = self._check_card()
        if not card_status:
            raise ValueError("Card could not be found or created.")
        if self.card.comment:
            comment_status = self._add_comment()
            if not comment_status:
                raise ValueError("Comment could not be added.")
        return True

    def _add_comment(self):
        endpoint = f"/cards/{self.card.id}/actions/comments"
        payload = {"text": self.card.comment}
        success, _ = self._post(endpoint, payload)
        if success:
            return True
        return False

    def _check_board(self):
        if (board_id := self.board.id):
            endpoint = "/boards/board_id"
            success, _ = self._get(endpoint)
            if not success:
                return self._create_board()
            else:
                return True
        elif (board_name := self.board.name):
            endpoint = "/members/me/boards?fields=id,name"
            success, resp = self._get(endpoint)
            if success:
                board_id = [x.get("id") for x in
                            resp if x.get("name") == board_name]
                if board_id:
                    self.board.id = board_id[0]
                    return True
                else:
                    return self._create_board()
            else:
                return False

    def _create_board(self):
        endpoint = "/boards"
        payload = self.board.payload()
        success, resp = self._post(endpoint, payload)
        if success:
            self.board.id = resp.get("id")
            return True
        return False

    # TODO: check if card exists before creating it
    def _check_card(self):
        return self._create_card()

    def _create_card(self):
        endpoint = "/cards"
        payload = self.card.payload()
        success, resp = self._post(endpoint, payload)
        if success:
            self.card.id = resp.get("id")
            self.card.url = resp.get("url")
            return True
        return False

    def _check_labels(self):
        endpoint = f"/boards/{self.board.id}/labels?fields=id,name"
        success, resp = self._get(endpoint)
        if success:
            label_ids = [x.get("id") for x in
                         resp if x.get("name") in self.card.label_names]
            self.card.label_ids += label_ids
            new_labels = [x for x in self.card.label_names
                          if x not in [y.get("name") for y in resp]]
            new_ids = self._create_labels(new_labels)
            self.card.label_ids += new_ids
            return True
        return False

    # TODO: Error handling for labels
    def _create_labels(self, new_labels):
        colors = ["yellow", "purple", "blue", "red", "green",
                  "orange", "black", "sky", "pink", "lime"]
        endpoint = "/labels"
        label_ids = []
        for label in new_labels:
            payload = {
                "name": label,
                "color": random.choice(colors),
                "idBoard": self.board.id,
            }
            success, resp = self._post(endpoint, payload)
            if success:
                label_ids.append(resp.get("id"))
        return label_ids

    def _check_list(self):
        lists = self._get_lists_on_board()
        if (list_id := self.list.id):
            if [x for x in lists if x.get("id") == list_id]:
                return True
        if (name := self.list.name):
            if (l_id := [x.get("id") for x in lists if x.get("name") == name]):
                self.list.id = l_id[0]
                return True
            else:
                return self._create_list()
        else:
            return False

    def _create_list(self):
        endpoint = "/lists"
        payload = self.list.payload()
        success, resp = self._post(endpoint, payload)
        if success:
            self.list.id = resp.get("id")
            return True

    def _get(self, endpoint, payload={}):
        payload["key"] = self.key
        payload["token"] = self.token
        resp = requests.request(
            "GET",
            f"{self.url_base}{endpoint}",
            headers=self.headers,
            params=payload,
        )
        if resp.status_code == 200:
            return True, resp.json()
        else:
            return False, ""

    def _get_lists_on_board(self):
        endpoint = f"/boards/{self.board.id}/lists?fields=id,name"
        success, resp = self._get(endpoint)
        if success:
            return resp
        else:
            return []

    def _post(self, endpoint, payload):
        payload["key"] = self.key
        payload["token"] = self.token
        resp = requests.request(
            "POST",
            f"{self.url_base}{endpoint}",
            headers=self.headers,
            params=payload,
        )
        if resp.status_code == 200:
            return True, resp.json()
        else:
            return False, ""
