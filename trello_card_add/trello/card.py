class Card:
    def __init__(self, lst, name="", desc="", label_names=[], comment=""):
        self.list = lst
        self.name = name
        self.desc = desc
        self.label_names = label_names
        self.label_ids = []
        self.comment = comment

    def payload(self):
        payload = {"idList": self.list.id}
        if self.name:
            payload["name"] = self.name
        if self.desc:
            payload["desc"] = self.desc
        if self.label_ids:
            payload["idLabels"] = self.label_ids
        return payload
