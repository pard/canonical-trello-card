import argparse
from pathlib import Path
from ruamel.yaml import YAML
from trello.trello import Trello
from trello.board import Board
from trello.card import Card
from trello.lst import List


def get_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-b",
                        "--board",
                        type=str,
                        required=True,
                        dest="board_name",
                        help="Board name")
    parser.add_argument("-l",
                        "--list",
                        type=str,
                        required=True,
                        dest="list_name",
                        help="List name")
    parser.add_argument("-c",
                        "--card",
                        type=str,
                        required=True,
                        dest="card_name",
                        help="Card name")
    parser.add_argument("-d",
                        "--desc",
                        type=str,
                        default="",
                        dest="card_desc",
                        help="Card description")
    parser.add_argument("-L",
                        "--labels",
                        type=str,
                        nargs="+",
                        default=[],
                        dest="card_labels",
                        help="Card labels")
    parser.add_argument("-C",
                        "--comment",
                        type=str,
                        default="",
                        dest="card_comment",
                        help="Card comment")
    parser.add_argument("-s",
                        "--settings",
                        type=str,
                        default="settings.yml",
                        dest="settings_file",
                        help="Settings file")
    return parser.parse_args()


def parse_settings(path):
    yaml = YAML(typ='safe')
    return yaml.load(path)


def main():
    args = get_args()
    settings = parse_settings(Path(args.settings_file)).get("trello_card_add")
    board = Board(name=args.board_name)
    lst = List(args.list_name, board)
    card = Card(lst,
                name=args.card_name,
                desc=args.card_desc,
                label_names=args.card_labels,
                comment=args.card_comment)
    trello = Trello(settings.get("key"), settings.get("token"))
    card_status = trello.add_card(board, lst, card)
    if card_status:
        print("Your new Trello card was created!\n"
              f"View it at: {trello.card.url}")


if __name__ == "__main__":
    main()
